﻿using Meep.Tech.Games.Basic.Geometry;

namespace UnityEngine {

  /// <summary>
  /// Extensions for 2D coordinates and unity vectors
  /// </summary>
  public static class D2CoordinateExtensions {

    #region Vector Conversion

    /// <summary>
    /// Create a coordinate from a vec3.
    /// </summary>
    public static Coordinate Coord2D(this Vector3 coordinate) {
      return new Coordinate((int)coordinate.x, (int)coordinate.z);
    }

    /// <summary>
    /// Create a vec3 form a 2D coordinate
    /// </summary>
    public static Vector3 Vec3(this Coordinate coordinate) {
      return new Vector3(coordinate.x, 0, coordinate.z);
    }

    /// <summary>
    /// Create a coordinate from a vec3.
    /// </summary>
    public static Coordinate Coord2D(this Vector2 coordinate) {
      return new Coordinate((int)coordinate.x, (int)coordinate.y);
    }

    /// <summary>
    /// Create a vec3 form a 2D coordinate
    /// </summary>
    public static Vector2 Vec2(this Coordinate coordinate) {
      return new Vector2(coordinate.x, coordinate.z);
    }

    #endregion
  }
}